AWS :cloud:, Pulumi :wrench:, Python :snake:, Docker :whale: and CI/CD :fox:

**Intro**

this project is about:
- building a simple flask webapp docker image using Dockerfile
- push the image to gitlab registry
- install pulumi v2.15.6, python3.8 and awscli v2
- preview (dry run) infrastructure deployment of a full Amazon Elastic Container Service (ECS) "Fargate" cluster and related infrastructure
- if successful, it will run the deployment in place.

**Architecture**

![](architecture.png)

**Notes:** :speech_balloon:

I have used AWS Sandbox account for the purpuse of this project.

Feel free to run `pulumi destroy --yes` command to delete all of deployed infrastructure.

Some CI/CD variables needs to be defined in order for the pipeline to run successfully
