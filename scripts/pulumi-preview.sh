#!/bin/bash

# exit if a command returns a non-zero exit code
# print the commands and their args as they are executed
set -e -x

# Add the pulumi CLI to the PATH
export PATH=$PATH:$HOME/.pulumi/bin

# AWS creds here - variables with $ to be set on gitlab var env
aws configure set default.region $AWS_DEFAULT_REGION
aws configure set aws_access_key_id '$AWS_ACCESS_KEY_ID'
aws configure set aws_secret_access_key '$AWS_SECRET_ACCESS_KEY'

pulumi stack select infra

# preview configuration before deployment
pulumi preview
